const axios = require('axios');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
//lista operationType
var operationType = [];
//lista id discipline
discipline = [];
//faccio richiesta http all'url per ottenere il json
axios.get('https://backend-staging.epicuramed.it/operationtasks').then((response) => {
    var obj = response.data;
    //per ogni elemento del json
    for (var element in obj){
        //aggiungo elemento "operationType" all'array
        operationType.push(obj[element].operationType);
    }
}).then(()=>{
    //popolo operationType
    MongoClient.connect(url, (err, db)=> {
        //se errore stampo errore
        if (err){
            console.error(err);
        }
        //connetto a "mydb"
        var dbo = db.db("mydb");
        //per ogni elemento in operationType
        for (var i in operationType){
            operationType[i]
            //aggiungo operationType al db
            try{
                    query = {_id:operationType[i]._id}
                    dbo.collection("operationType").insertOne(operationType[i],function(err,res){
                        //se l'oggetto è già presente
                        if(err){
                            //lo inserisco generando un nuovo id
                            delete operationType[i]._id
                            dbo.collection("operationType").insertOne(operationType[i]);
                        }
                    });
                    console.log("operation type aggiunta");
            }
            catch(e){
                console.error(e);
            }
            //per ogni operationType
            //popolo lista discipline con gli id e il risultato della chiamata http
            axios.get('https://backend-staging.epicuramed.it/disciplines/'+operationType[i].discipline).then((res) => {
                resBody = res.data; 
                dizionario = {}
                dizionario[operationType[i].discipline] = resBody;
                //aggiungo elementi a collezione discipline
                try{
                    dbo.collection("discipline").insertOne(dizionario);
                    console.log("disciplina aggiunta");
                }
                catch(e){
                    console.error(e);
                }
            });
        }
    });
})